from app import app, db
from core.chatbot import models
from core.chatbot import routes
import sys


@app.shell_context_processor
def make_shell_context():
    raul = ""
    return {
        'db': db,
        'models': models,
    }


if __name__ == '__main__':
    sys.path.append("/bot/src")
    app.run(host='0.0.0.0', port=80, debug=True)
