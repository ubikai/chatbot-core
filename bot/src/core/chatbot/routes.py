from app import app
from flask import request
from core.chatbot.respond import start_answer
from core.chatbot.models import User, DataCategory, Data
from sqlalchemy.orm.attributes import flag_modified
import os, sys, traceback


@app.route("/")
def index():
    return {"INSTALLED": "core_chatbot"}


@app.route("/facebook", methods=['GET', 'POST'])
def receive_message():
    if request.method == 'GET':
        """Before allowing people to message your bot, Facebook has implemented a verify token
        that confirms all requests that your bot receives came from Facebook."""
        token_sent = request.args.get("hub.verify_token")
        if token_sent == app.config['VERIFY_TOKEN']:
            return request.args.get("hub.challenge")
        return {"STATUS": "OK"}
    else:
        output = request.get_json()
        try:
            print("request", os.getpid(), " param:", request.get_json())
            if output['entry']:
                for event in output['entry']:
                    if event.get('messaging'):
                        messaging = event['messaging']
                        for message in messaging:
                            recipient_id = message['sender']['id']

                            user = User.get_user(recipient_id)

                            user.add_analytics('interaction')

                            if message.get('message'):
                                # Facebook Messenger ID for user so we know where to send response back to
                                if message['message'].get('text'):
                                    if message['message'].get('quick_reply'):
                                        payload = message['message']['quick_reply']['payload']
                                        message = {
                                            'message': message['message']['text'],
                                            "payload": payload
                                        }
                                        start_answer(user, message)
                                    else:
                                        message = {
                                            'message': message['message']['text'],
                                            "payload": 0
                                        }
                                        start_answer(user, message)
                                # if user sends us a GIF, photo,video, or any other non-text item
                                # if message['message'].get('attachments'):
                                # process the attachment and respond
                            elif message.get('postback'):
                                if message['postback'].get("payload"):
                                    if message['postback'].get('referral'):
                                        ad_campain = message['postback']['referral'].get('ref')
                                        user.add_analytics(ad_campain)
                                        ad_id = message['postback']['referral'].get('ad_id')
                                        user.data.append(
                                            Data(category=DataCategory.get_data_category("ad"), keyword='ad_id',
                                                 value=ad_id))
                                        user.submit()
                                    message = {
                                        'message': message['postback']['title'],
                                        "payload": message['postback']['payload']
                                    }
                                    start_answer(user, message)
        except Exception as ex:
            print("[routes] - {} - Error - ".format(os.getpid()), ex, sys.exc_info(), traceback.format_exc())
    print("end request", os.getpid())
    return {"STATUS": "Message Processed"}


@app.route("/facebook/special_flow", methods=['POST'])
def fb_check_work():
    #TODO: CLEAN THIS SHIT
    """
    data{
        user_id: id,
        action: str,
        temp: {
            keyword: str,
            value: any
        }
    }
    :return:
    """
    try:
        data = request.get_json()
        if data:

            user_id = data.get('user_id')
            action = data.get('action')
            temp = data.get('temp')

            if user_id and action:
                user = User.query.get(user_id)
                if user:
                    if temp:
                        keyword = temp.get('keyword')
                        value = temp.get('value')

                        if keyword and value is not None:
                                user.temp[keyword] = value
                        flag_modified(user, 'temp')
                        user.submit()

                    message = {
                        'message': action,
                        "payload": 0
                    }
                    start_answer(user, message, is_special_flow=True)
                else:
                    print("Cannot find user in DB")
                    return {"ERROR": "Cannot find user in DB"}
            else:
                print("Missing user_id or action")
                return {"ERROR": "Missing user_id or action"}
    except Exception as ex:
        print("[routes] - {} - Error - ".format(os.getpid()), ex, sys.exc_info(), traceback.format_exc(), file=sys.stderr)
        return {"ERROR": "Check the log file for more info"}
    return {"STATUS": "Message Started"}


@app.route("/refresh_flows", methods=['GET'])
def refresh_flows():
    from core.memcache.flows_manager import load_flows

    try:
        load_flows()
        print('Flows Loaded Successfully')
    except Exception as ex:
        print('Flows failed to load')
        print("Error Info: ", ex)
        return {"ERROR": "Cannot load flows"}

    return {"STATUS": "Flows loaded!"}


@app.route("/broadcast", methods=['POST'])
def broadcast():
    """
    :param: data {
        user_id: int,
        message: str,
        message_type: str,  <'message', 'input' or 'quickreplies>
        buttons: list  <only if message_type=='quickreplies'>
    }
    """
    data = request.get_json()
    if data:
        user_id = data.get('user_id')
        message = data.get('message')
        m_type = data.get('message_type')
        buttons = data.get('buttons')

        if user_id and message and m_type:
            user = User.query.get(user_id)
            user.flow.reset()

            if not user.temp.get('broadcast'):
                user.temp['broadcast'] = {'broadcast': 1}
            user.temp['broadcast'] = {
                'message': message,
                'message_type': m_type,
                'buttons': buttons
            }
            flag_modified(user, "temp")
            user.submit()
            print('[broadcast route]', user.temp, file=sys.stderr)
            message = {'message': 'broadcast', "payload": 0}
            start_answer(user, message)  # is_special_flow=True (???)
        else:
            return {"ERROR": 'Some values are None (user={}, message={}, m_type={})' \
                .format(user_id, message, m_type)}
    else:
        return {"ERROR": 'Data not exist-erino'}
    return {"STATUS": "Message Started"}


@app.route("/user")
def user():
    fb_id = request.args.get('fb_id')
    bot_id = request.args.get('bot_id')

    if fb_id:
        u = User.query.filter(User.fb_id == fb_id).first()

        if not u:
            return {'ERROR': 'there is no user with that fb_id'}

        return {'user_id': u.id, 'first_name': u.first_name, 'last_name': u.last_name, 'profile_pic': u.profile_pic}

    elif bot_id:
        u = User.query.get(int(bot_id))

        if not u:
            return {'ERROR': 'there is no user with that id'}
        return {'user_id': u.id, 'first_name': u.first_name, 'last_name': u.last_name, 'profile_pic': u.profile_pic}

    return {'ERROR': 'Missing fb_id or bot_id'}


@app.route("/logs", methods=['GET'])
def get_logs():
    data = ""
    try:
        with open('/bot/logs/error.log', 'r') as f:
            data = f.read()
    except Exception as ex:
        print("LOGS ERROR: ", ex, file=sys.stderr)
        return {'ERROR': "io-am incercat, da' nu a vrut."}
    return {'LOGS': data}
