from app import db
from sqlalchemy.orm.attributes import flag_modified


class BaseMixin(object):

    def submit(self):
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        try:
            db.session.add(obj)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())


class User(BaseMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    fb_id = db.Column(db.String(64), unique=True)
    temp = db.Column(db.JSON, default={})
    first_name = db.Column(db.String(64, collation='utf8_romanian_ci'))
    last_name = db.Column(db.String(64, collation='utf8_romanian_ci'))
    gender = db.Column(db.String(64))
    profile_pic = db.Column(db.String(128))
    flow = db.relationship('Flow', uselist=False, back_populates='user', cascade="all, delete, delete-orphan")
    analytics = db.relationship("Analytics", back_populates='user')
    data = db.relationship("Data", back_populates='user')

    def __repr__(self):
        return '<User id={}, name={}, fb_id={}>'.format(self.id, self.first_name + ' ' + self.last_name, self.fb_id)

    @staticmethod
    def create_user(u_id):

        from core.facebook.messages import get_user_data

        user_data = get_user_data(u_id)
        user = User(
            fb_id=u_id,
            first_name=user_data.get('first_name') or None,
            last_name=user_data.get('last_name') or None,
            gender=user_data.get('gender') or None,
            profile_pic=user_data.get('profile_pic') or None
        )
        return user

    @staticmethod
    def get_user(u_id):
        user = User.query.filter(User.fb_id == u_id).first()
        if not user:
            user = User.create_user(u_id)
            user.flow = Flow(user=user)
            user.submit()
            user = User.query.filter(User.fb_id == u_id).first()
        return user

    def add_analytics(self, analytics_type_name):
        analytics_type = AnalyticsType.get_analytics_type(analytics_type_name)
        entry = db.session.query(Analytics).filter(Analytics.user_id==self.id).filter(Analytics.analytics_type_id==analytics_type.id).first()

        if not entry:
            entry = Analytics()
            entry.user = self
            entry.analytics_type = AnalyticsType.get_analytics_type(analytics_type_name)
            entry.value = 0

        entry.value += 1

        return entry


class Flow(BaseMixin, db.Model):
    __tablename__ = 'flow_data'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), unique=True)
    temp = db.Column(db.String(1024, collation='utf8_romanian_ci'), default=None)
    current_flow = db.Column(db.String(64), default="default.fallback")
    current_state = db.Column(db.Integer(), default=0)
    next_state = db.Column(db.JSON, default=[0])
    flows_completed = db.Column(db.JSON, default=None)
    field_type = db.Column(db.String(64), default=None)
    expect_from_user = db.Column(db.String(64), default=None)
    user = db.relationship("User", back_populates='flow')
    flow_content = None

    def __repr__(self):
        return '<Flow user={},user_id={}, ' \
               'current_flow={}, ' \
               'expect_from_user={}, ' \
               'current_state={}, ' \
               'next_state={}, ' \
               'flows_completed={}, ' \
               'field_type={}>, '.format(
                    self.user,
                    self.user_id,
                    self.current_flow,
                    self.expect_from_user,
                    self.current_state,
                    self.next_state,
                    self.flows_completed,
                    self.field_type,
                )

    def reset(self):
        print("[reset] Reset flow data")
        self.temp = None
        self.current_flow = "default.fallback"
        self.current_state = 0
        self.next_state = [0]
        self.field_type = None
        self.expect_from_user = None

    def error(self):
        self.reset()
        self.current_flow = "default.error"

    def update_flows_completed(self, value):
        if 'default' not in value:
            tab = self.flows_completed
            edit = False
            if not tab:
                tab = []
            tab_len = len(tab)
            for i in range(tab_len):
                if value == tab[i]:
                    del tab[i]
                    tab.append(value)
                    edit = True
            if not edit:
                tab.append(value)
            flag_modified(self, "flows_completed")
            self.flows_completed = tab

    def get_flow_content(self):
        from core.memcache import get_flow
        flow = get_flow(self.current_flow)
        self.flow_content = flow
        return flow


class Attachment(BaseMixin, db.Model):
    __tablename__ = 'fb_attachments'

    id = db.Column(db.Integer, primary_key=True)
    att_id = db.Column(db.String(64), unique=True)
    att_url = db.Column(db.String(128))

    def __repr__(self):
        return '<Attachment id={}, att_id, att_url={}>'.format(self.id, self.att_id, self.att_url)


class AnalyticsType(BaseMixin, db.Model):
    __tablename__ = "analytic_types"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    entries = db.relationship("Analytics", back_populates="analytics_type")

    @staticmethod
    def get_analytics_type(analytics_type_name):
        analytics_type = AnalyticsType.query.filter(AnalyticsType.name==analytics_type_name).first()
        if not analytics_type:
            analytics_type = AnalyticsType(name=analytics_type_name)
        return analytics_type


class Analytics(BaseMixin, db.Model):
    __tablename__ = 'analytics'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    analytics_type_id = db.Column(db.Integer, db.ForeignKey('analytic_types.id'))
    value = db.Column(db.Integer, default=0)
    user = db.relationship("User", back_populates='analytics')
    analytics_type = db.relationship("AnalyticsType", back_populates='entries')

    def __repr__(self):
        return '<Analytics id={}, type_name={}, value={}>'.format(self.id, self.analytics_type.name, self.value)


class DataCategory(BaseMixin, db.Model):
    __tablename__ = 'data_categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)

    def __repr__(self):
        return "<DataCategory id={}, name={}>".format(self.id, self.name)

    @staticmethod
    def get_data_category(category_name):
        category_name = category_name.lower()
        data_category = DataCategory.query.filter(DataCategory.name==category_name).first()
        if not data_category:
            data_category = DataCategory(name=category_name)
        return data_category


class Data(BaseMixin, db.Model):
    __tablename__ = "data"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('data_categories.id'))
    keyword = db.Column(db.String(64))
    value = db.Column(db.String(128))

    user = db.relationship("User", back_populates='data')
    category = db.relationship("DataCategory")

    def __repr__(self):
        return "<Data category={}, keyoword={}, value={}>".format(self.category.name, self.keyword, self.value)