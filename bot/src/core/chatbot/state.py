from core.default_field_types import *
from field_types import *
from helpers import DefaultError
from core.chatbot.message import format_content, api_format_content


class State:
    def __init__(self, state_data, flow_len, flow_name):
        self.index = state_data["index"]
        # TODO: make a hard copy of the content
        self.content = state_data["content"]
        self.next_state = state_data["next_state"]
        self.field_type = state_data["field_type"]
        self.expect_from_user = state_data["expect_from_user"]
        self.delay = state_data["delay"]
        self.flow_length = flow_len
        self.flow_name = flow_name
        self.format_state()

    def __repr__(self):
        return "<State index={}," \
               "next_state={}, " \
               "expect_from_user={}, " \
               "flow_name={}>".format(
            self.index,
            self.next_state,
            self.expect_from_user,
            self.flow_name
        )

    def format_state(self):
        # format next state list
        if 0 in self.next_state or -1 in self.next_state:
            for i in range(len(self.next_state)):
                if self.next_state[i] == -1:
                    self.next_state[i] = 9999999
                elif self.next_state[i] == 0:
                    self.next_state[i] = self.index + 1

    def send_message(self, user, **kwarg):
        try:
            format_content(self.content, user)
            api_format_content(self.content, user)
            eval(self.field_type)(self, user, **kwarg)
        except NameError as err:
            raise DefaultError("cannot find the field type named {}".format(self.field_type))
        except KeyError as err:
            raise DefaultError("cannot find the key {} in your flow content!".format(err))

    def get_next_state(self, flow_content):
        if self.next_state[0] < self.flow_length:
            next_state = State(flow_content[self.next_state[0]], self.flow_length, self.flow_name)
        else:
            next_state = None
        return next_state

    def change_state(self, user, flow_content, index):
        # get new state
        self.__init__(flow_content[user.flow.current_state], len(flow_content), user.flow.current_flow)
        self.next_state = [index]
        self.expect_from_user = None
