from core.chatbot.state import State
from core.chatbot.intent import Intent
from core.memcache import *
from core.google.api_ai import detect_intent
from core.google.ro_swearing import swearing
from multiprocessing import Process
from helpers import DefaultError
from sqlalchemy.orm.attributes import flag_modified
from time import sleep
import os
from components import *
from core.components import *

special_intents = {
    'stop': 'default.stop',
    'default.stop': 'default.stop',
}

MODE = 'n'


def respond(user, message):
    """

    :param user: SQLAlchemy Object
    :param message: Ex: {'message': 'greeting', 'payload': 0}
    :return:
    """
    engage_user(user.fb_id)  # set user in memcached -> set(fb_id, False)
    try:
        flow_data = user.flow
        user.flow.get_flow_content()  # feed Flow.flow_content with user.flow.current_flow from memcached

        print('[respond] BEFORE user.get_flow_content:', flow_data.current_flow)
        if not user.flow.flow_content:
            raise DefaultError("cannot get flow content for {}".format(flow_data.current_flow))
        # feed state with the state from flow depending on current_state
        state = State(
            user.flow.flow_content[flow_data.current_state],
            len(user.flow.flow_content),
            flow_data.current_flow
        )

        print("[respond] PID: ", os.getpid(), "State -> ", state)

        if user.flow.expect_from_user:
            exec_component(flow_data, message, state, user)
            if state.expect_from_user is None:
                state = state.get_next_state(user.flow.flow_content)
        else:
            intent = Intent(state, user, message)
            user.flow.flow_content = intent.get_flow_data()

            # user are in a core.flow, e.g default.fallback
            if "default" in user.flow.current_flow:
                inc_counter(user.fb_id)
            else:
                del_counter(user.fb_id)
            print("[respond] COUNTER: ", get_counter(user.fb_id))

            # anti spam stupid shit
            if int(get_counter(user.fb_id)) >= 3 and os.environ.get("UNIT_TESTING") != "1":
                if os.environ.get('FALLBACK_MENU', 0):
                    message['message'] = 'menu'
                    intent = Intent(state, user, message)
                    user.flow.flow_content = intent.get_flow_data()
                    del_counter(user.fb_id)
                else:
                    user.flow.current_flow = 'default.fallback'
                    return

            # do not respond if
            if "neg_" in user.flow.current_flow:
                user.flow.current_flow = 'default.fallback'
                return

            if os.environ.get('LANG_CODE', 'en') == 'ro':
                for item in message['message'].lower().split(' '):
                    if item in swearing:
                        return

            if not user.flow.flow_content:
                raise DefaultError("cannot get flow content for {}".format(flow_data.current_flow))
            state = State(user.flow.flow_content[0], len(user.flow.flow_content), user.flow.current_flow)
        while cont(state, user):
            state.send_message(user, message=message)
            if state.expect_from_user:
                break
            state = state.get_next_state(user.flow.flow_content)

    except DefaultError as err:
        print("[respond] Error: ", err.message)
        user.flow.error()
        user.flow.flow_content = get_flow(user.flow.current_flow)
        state = State(
            user.flow.flow_content[user.flow.current_state],
            len(user.flow.flow_content),
            user.flow.current_flow
        )
        state.send_message(user, error_data=err.message)
    finally:
        print("[respond] final User.flow -> ", user.flow)
        user.submit()
        disengage_user(user.fb_id)


def update_flow_data(flow_data, state):
    flow_data.current_state = state.index
    flow_data.next_state = state.next_state
    # mark as modified for SQLAlchemy
    flag_modified(flow_data, "next_state")
    flow_data.field_type = state.field_type
    flow_data.expect_from_user = state.expect_from_user


def cont(state, user):
    if not state:
        print("[cont] Flow has ended!")
        user.flow.update_flows_completed(user.flow.current_flow)
        user.flow.reset()
        return False
    elif check_to_disengage(user.fb_id):
        # Stop sending message because a stop signal has been sent
        update_flow_data(user.flow, state)
        print("disengage", check_to_disengage(user.fb_id))
        return False
    else:
        update_flow_data(user.flow, state)
        return True


def exec_component(flow_data, message, state, user):
    try:
        print("[exec_component] executing compnent {}".format(flow_data.expect_from_user))
        eval(flow_data.expect_from_user)(message, user, state)
    except NameError as ne:
        print('[exec_component]', ne)
        raise DefaultError('cannot find the component named {}'.format(flow_data.expect_from_user))
    except Exception as ex:
        import os, sys, traceback
        print("[exec_component] - PID={} - User.id={} - Error - ".format(os.getpid(), user.id), sys.exc_info(), ex, traceback.format_exc())
        if ex.__class__ == DefaultError:
            raise DefaultError("cannot execute component {} ".format(flow_data.expect_from_user) + "because I {} ".format(ex.message))
        else:
            raise DefaultError("cannot execute component {}".format(flow_data.expect_from_user))


def special_answer(user, message):
    mark_to_disengage(user.fb_id)
    while user_engaged(user.fb_id):
        print("user engaged")
        sleep(0.2)
        pass
    # db.session.refresh(user)
    if special_intents.get(message["message"]):
        user.flow.reset()
        user.flow.current_flow = special_intents[message["message"]]
        message["message"] = special_intents[message["message"]]
    else:
        user.flow.reset()
    return message


def special_payload(user, message):
    mark_to_disengage(user.fb_id)
    while user_engaged(user.fb_id):
        print("user engaged")
        sleep(0.2)
        pass
    # db.session.refresh(user)
    user.flow.reset()
    user.flow.current_flow = message["payload"]
    message["message"] = message["payload"]
    return message


def start_answer(user, message, is_special_flow=False):
    # REFACTOR:
    #   add_analytics('interaction') mai e o data in routes.py/facebook + ca scrie "INTER CATION" an peleme
    #   special_answer & special_payload sunt in mare parte identice
    if os.environ.get("UNIT_TESTING") and os.environ.get("UNIT_TESTING") == "1":
        user.add_analytics('interaction')
    print("[start_answer] Starting asnwer for ", message)

    if get_flow(message['payload']):
        message = special_payload(user, message)
    elif message["message"] in special_intents or is_special_flow:
        message = special_answer(user, message)
    else:
        api = detect_intent(user.fb_id, message["message"])
        if api in special_intents:
            message["message"] = api
            message = special_answer(user, message)
        else:
            if user_engaged(user.fb_id):
                return
            else:
                pass

    print('[start_answer] BEFORE respond() current_flow={}, message={}'.format(user.flow.current_flow, message))

    if MODE == 'n':
        print("[start_answer] Mode is set on Normal")
        respond(user, message)
    elif MODE == 'p':
        print("[start_answer] Mode is set on Process")
        t = Process(target=respond, args=(user, message))
        t.start()
