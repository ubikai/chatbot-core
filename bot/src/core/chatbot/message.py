import re
from datetime import datetime
from helpers import DefaultError
import requests
from app import app

def format_content(content, user):

    try:
        if content.get('url'):
            for key, value in content['url'].items():
                content['url'][key] = replace_message(value, user)
        if content.get('text'):
            content['text'] = replace_message(content['text'], user)
            if content.get('buttons'):
                for i in range(len(content['buttons'])):
                    content['buttons'][i] = replace_message(content['buttons'][i], user)
            return content
        if content.get('text_variation'):
            for i in range(len(content.get('text_variation'))):
                content['text_variation'][i] = replace_message(content['text_variation'][i], user)
            if content.get('buttons'):
                for i in range(len(content['buttons'])):
                    content['buttons'][i] = replace_message(content['buttons'][i], user)
            return content
    except AttributeError:
        pass
    return content


def replace_message(content, user):
    print("[replace_message] FORMATING TEXT")
    item_to_replace = [
        "\{\{.*?\}\}"
    ]

    for regex in range(len(item_to_replace)):
        while True:
            item = re.search(item_to_replace[regex], content)
            if not item:
                break
            var_name = item.group()
            var = var_name.strip("{} ")
            try:
                var = eval(var)
            except (NameError, SyntaxError, KeyError, AttributeError) as ex:
                var = "<undefined>"
            content = content.replace(var_name, str(var))
    return content

endpoints = {
    'user': {'scheduler_time': 1548180136.0, 'id': 2, 'bot_id': 1}
}


def get_api_data(endpoint, attr, user):
    out = endpoints.get(endpoint)
    data = {
        'user_id': user.id,
    }

    out = requests.get("http://"+app.config['TT_IP']+"/user", params=data)

    if out.status_code is not 200:
        raise DefaultError("cannot access API")

    out = out.json()
    attr_data = out.get(attr)
    print(attr_data)
    if not attr_data:
        raise DefaultError("cannot find {}".format(attr))

    return attr_data


def special_attr(attr, var):
    if "time" in attr:
        date = datetime.fromtimestamp(var)
        var = "{:02d}:{:02d}".format(date.hour, date.minute)
    return var


def format_api_data(text, user):

    regex = "\<\<.*?\>\>"

    while True:
        item = re.search(regex, text)
        if not item:
            break
        var_name = item.group()
        var = var_name.strip("<> ")
        params = var.split('.')

        try:
            if len(params) != 2:
                raise Exception

            endpoint = params[0]
            attr = params[1]
            var = get_api_data(endpoint, attr, user)
            var = special_attr(attr, var)
        except (NameError, SyntaxError, KeyError, AttributeError, Exception) as ex:
            print(ex)
            var = "<undefined>"
        text = text.replace(var_name, str(var))
    return text


def api_format_content(content, user):
    try:
        if content.get('text'):
            content['text'] = format_api_data(content['text'], user)
            if content.get('buttons'):
                for i in range(len(content['buttons'])):
                    content['buttons'][i] = format_api_data(content['buttons'][i], user)
            return content
    except AttributeError:
        pass
    return content
