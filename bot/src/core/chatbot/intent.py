from core.memcache import get_flow
from core.google.api_ai import detect_intent


class Intent:
    def __init__(self, state, user, message):
        self.intent = None
        self.message = message['message']
        self.payload = message.get('payload')
        self.state = state
        self.user = user

    def get_intent(self):
        # check if user typed exactly the name of the flow
        if self.message and " " not in self.message and self.payload != "GET_STARTED":
            flow = get_flow(self.message)
            if flow:
                return self.message
        # check if this is the first interaction with the bot on fb
        if self.payload == "GET_STARTED":
            print("Switch to get started!")
            return 'get_started'
        # check if api.ai finds something about the message
        return detect_intent(self.user.id, self.message)

    def get_flow_data(self):
        self.user.flow.reset()
        self.user.flow.current_flow = self.get_intent()
        flow_content = get_flow(self.user.flow.current_flow)

        if not flow_content:
            self.user.flow.current_flow = 'default.fallback'
            flow_content = get_flow(self.user.flow.current_flow)

        return flow_content
