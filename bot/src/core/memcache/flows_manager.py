from pymemcache.client import base
from os import listdir
from time import sleep
import etcd3

client = base.Client(('memcached_flows', 11211))

etcd3_client = etcd3.client(host='etcd3', port=2379)

lock = etcd3_client.lock("flow")

def load_default_flows():
    # TOneverDO: ConnectionRefusedError(incorrect port), socket.gaierror(service is down or name is misspelled)
    """
    loads all flows in memcache
    :param:
    :return: boolean
    """
    from core.default_flows import contents
    print('='*60)
    print("DEFAULT FLOWS FOUND:")
    for key in contents.keys():
        print('---', key)
    print('='*60)
    return client.set_multi(contents)


def load_custom_flows():
    # TOneverDO: ConnectionRefusedError(incorrect port), socket.gaierror(service is down or name is misspelled)
    """
    loads all flows in memcache
    :param:
    :return: boolean
    """
    from flows import contents
    print('='*60)
    print("CUSTOM FLOWS FOUND:")
    for key in contents.keys():
        print('---', key)
    print('='*60)
    return client.set_multi(contents)


def load_flows():
    load_default_flows()
    load_custom_flows()


def get_flow(flow_name):
    """
    retrieve a flow from memcache
    :param flow_name:
    :return: flow_content
    """
    # sometimes the payload contains the flow name so we have to check for that first
    # only the default value is int<0> others are all strings str<0> or str<greeting>
    # we could have written != 0
    if type(flow_name) == str:
        out = None

        lock.acquire()
        try:
            out = client.get(flow_name.encode("utf-8"))
        finally:
            lock.release()

        if out:
            return eval(out.decode("utf-8"))
    return None
