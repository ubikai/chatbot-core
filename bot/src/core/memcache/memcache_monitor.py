import re, telnetlib, sys, os
from time import sleep
from pymemcache.client import base

class MemcachedStats:

    _client = None
    _key_regex = re.compile('ITEM (.*) \[(.*); (.*)\]')
    _slab_regex = re.compile('STAT items:(.*):number')
    _stat_regex = re.compile("STAT (.*) (.*)\r")

    def __init__(self, host='localhost', port='11211', timeout=None):
        self._host = host
        self._port = port
        self._timeout = timeout

    @property
    def client(self):
        if self._client is None:
            self._client = telnetlib.Telnet(self._host, self._port,
                                            self._timeout)
        return self._client

    def command(self, cmd):
        ' Write a command to telnet and return the response '
        self.client.write(("%s\n" % cmd).encode('ascii'))
        return self.client.read_until(b'END').decode('ascii')

    def key_details(self, sort=True, limit=1):
        ' Return a list of tuples containing keys and details '
        cmd = 'stats cachedump %s %s'
        # print("slab_ids:", self.slab_ids())
        keys = [key for id in self.slab_ids()
            for key in self._key_regex.findall(self.command(cmd % (id, limit)))]
        if sort:
            return sorted(keys)
        else:
            return keys

    def keys(self, sort=True, limit=100):
        ' Return a list of keys in use '

        # print("key_details:", self.key_details(sort=sort, limit=limit))
        return [key[0] for key in self.key_details(sort=sort, limit=limit)]

    def slab_ids(self):
        ' Return a list of slab ids in use '
        ret = []
        out = self._slab_regex.findall(self.command('stats items'))
        for item in range(len(out)):
            if item % 4 == 0:
                ret.append(out[item])
        return ret

    def stats(self):
        ' Return a dict containing memcached stats '
        return dict(self._stat_regex.findall(self.command('stats')))


if __name__ == '__main__':
    stats = MemcachedStats('memcached_users', '11211')
    client = base.Client(('memcached_users', 11211))
    clear = lambda: os.system('clear')

    try:
        while True:
            clear()
            for key in stats.keys():
                val = client.get(key)
                if val:
                    print(key, " : ", eval(val))
            sleep(0.5)
    except KeyboardInterrupt:
        clear()
        quit()
