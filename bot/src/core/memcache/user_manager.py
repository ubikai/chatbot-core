from pymemcache.client import base
from time import sleep
import etcd3

client = base.Client(('memcached_users', 11211))
etcd3_client = etcd3.client(host='etcd3', port=2379)

lock = etcd3_client.lock("user")

def engage_user(user_id):
    """
    Adds in memcache user_id and False
    :param user_id:
    :return: None
    """
    lock.acquire()
    try:
        client.set(user_id, False)
    finally:
        lock.release()


def user_engaged(user_id):
    """
    Checks if the user is in cache
    :param user_id:
    :return bool:
    """
    out = None

    lock.acquire()
    try:
        out = client.get(user_id)
    finally:
        lock.release()

    return True if out else False


def disengage_user(user_id):
    """
    Deletes user from memcache
    :param user_id:
    :return:
    """
    lock.acquire()
    try:
        client.delete(user_id)
    finally:
        lock.release()


def mark_to_disengage(user_id):
    """
    Changes the user_id value to True
    :param user_id:
    :return:
    """
    out = None

    lock.acquire()
    try:
        out = client.get(user_id)
    finally:
        lock.release()

    if out:
        client.set(user_id, True)


def check_to_disengage(user_id):
    """
    Check if the user_id value is True
    :param user_id:
    :return bool:
    """

    out = None

    lock.acquire()
    try:
        out = client.get(user_id)
    finally:
        lock.release()
    if out:
        return eval(out)
    return False


def set_counter(user_id, counter=0):
    lock.acquire()
    try:
        client.set('count_'+str(user_id), counter, 1000)
    finally:
        lock.release()


def get_counter(user_id):
    out = None

    lock.acquire()
    try:
        out = client.get('count_'+user_id)
    finally:
        lock.release()
    if out:
        return eval(out)
    else:
        return 0


def inc_counter(user_id):
    counter = get_counter(user_id)
    if counter:
        counter = int(counter)
    else:
        counter = 0
    counter += 1
    set_counter(user_id, counter)


def del_counter(user_id):
    lock.acquire()
    try:
        client.delete('count_'+user_id)
    finally:
        lock.release()


if __name__ == '__main__':
    # set_counter('asdf')
    # print(get_counter('asdf'))
    inc_counter('asdf')
    print(get_counter('asdf'))
    # del_counter('asdf')
    # print(get_counter('asdf'))
