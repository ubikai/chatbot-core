import sys

sys.path[0] = '/bot/src'

from core.memcache.flows_manager import load_flows

try:
    load_flows()
    print('Flows Loaded Successfully')
except Exception as ex:
    print('Flows failed to load')
    print("Error Info: ", ex)
