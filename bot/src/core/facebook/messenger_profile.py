import sys
sys.path.append("/bot/src")

from core.facebook import *


def persistent_menu_settings():
    url = "https://graph.facebook.com/v2.6/me/messenger_profile"

    template = {
        "persistent_menu": [
            {
                "locale": "default",
                "composer_input_disabled": False,
                "call_to_actions": [
                    {
                        "title": "Restart Lesson",
                        "type": "postback",
                        "payload": "RESTART_FLOW"
                    },
                    {
                        "title": "Tutorial",
                        "type": "postback",
                        "payload": "TUTORIAL"
                    },
                    {
                        "title": "Stop Conversation",
                        "type": "postback",
                        "payload": "STOP"
                    },
                ]
            }
        ]
    }
    out = requests.post(url, json=template, params=auth_args)

    print("Status: ", out)
    if out:
        print("Response: ", out.json())


def welcome_screen():
    url = "https://graph.facebook.com/v2.6/me/messenger_profile"
    template = {
        "get_started": {
            "payload": "GET_STARTED"
        }
    }
    requests.post(url, json=template, params=auth_args)


if __name__ == '__main__':
    welcome_screen()
    persistent_menu_settings()
