import requests
from core.facebook import auth_args


def pass_handover_protocol(user_id):
    url = "https://graph.facebook.com/v2.6/me/pass_thread_control"

    data = {
        "recipient": {"id": user_id},
        "target_app_id": 263902037430900,
        # "metadata": "String to pass to secondary receiver app"
    }

    print(auth_args)
    print(data)

    out = requests.post(url, json=data, params=auth_args)
    print("[{}] FB_PASS_HANDOVER".format(out.status_code), end='')
    if out:
        print(" ->", out.json())


def take_handover_protocol(user_id):
    url = "https://graph.facebook.com/v2.6/me/take_thread_control"

    data = {
        "recipient": {"id": user_id},
    }

    out = requests.post(url, json=data, params=auth_args)
    print("[{}] FB_TAKE_HANDOVER".format(out.status_code), end='')
    if out:
        print(" ->", out.json())
