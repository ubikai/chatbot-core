# from facebook import *

from app import app
import requests

auth_args = {
  'access_token': app.config['ACCESS_TOKEN']
}

url = "https://graph.facebook.com/v2.6/me/messages"


def web_url_button(url_button, title):
    button = {
        "type": "web_url",
        "url": url_button,
        "title": title,
        "webview_height_ratio": "tall"
    }
    return button


def postback_button(text, id):
    button = {
        "type": "postback",
        "title": text,
        "payload": id
    }
    return button


def web_url_reply(title, payload_id, url=None):
    reply = {
        "content_type": "text",
        "title": title,
        "payload": payload_id,
        "image_url": url
    }
    return reply


def info_reply(text):
    reply = {
        "content_type": text
    }
    return reply


def get_user_data(recipient_id):
    """
    Retrieves user data from facebook
    :param recipient_id:
    :return: dict[with the requested field types]
    """
    get_user_data_url = "https://graph.facebook.com/v2.6/{}".format(recipient_id)
    fields = [
        'first_name',
        'last_name',
        'profile_pic'
        # 'locale',
        # 'timezone',
        # 'gender'
    ]
    req_params = {
        'fields': ','.join(fields),
        'access_token': auth_args['access_token']
    }
    out = requests.get(get_user_data_url, params=req_params)
    print("="*10)
    print("Facebook status get_user_data:", out)
    print("Details:", out.json())
    out = out.json()
    print("="*10)
    return out


def send_action(recipient_id, typing="typing_on"):
    """
    :Param type:
        mark_seen
        typing_on
        typing_off
    """
    message_template = {
        "recipient": {
            "id": recipient_id
        },
        "sender_action": typing
    }
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_ACTION".format(out.status_code), end='')
    if out:
        print(" ->", out.json())



# Buttons #
def send_button(recipient_id, content):
    buttons_list = []
    code = 0
    for button_text in content['buttons']:
        if content.get('url') and code in content['url']:
            buttons_list.append(web_url_button(content['url'][code],button_text))
        else:
            buttons_list.append(postback_button(button_text, code))
        code += 1
    message_template = {
        'recipient': {
            'id': recipient_id
        },
        'notification_type': 'REGULAR',
        "message":{
            "attachment":{
                "type":"template",
                "payload":{
                    "template_type":"button",
                    "text": content['text'],
                    "buttons": buttons_list
                }
            }
        }
    }
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_BUTTON".format(out.status_code), end='')
    if out:
        print(" ->", out.json())


def get_attachment_id(media_type, link, reusable=True):
    """
    Provides an attachment_id
    :param media_type:  "image"
    :param link:
    :return:
    """
    from core.chatbot.models import Attachment
    from app import db

    if reusable:
        attachment = Attachment.query.filter(Attachment.att_url == link).first()

        # check if attachment is in DB
        if attachment :
            return attachment.att_id

    # request fb to provide an attachment id for the media file

    message_template = {
        "message": {
            "attachment": {
                "type": media_type,
                "payload":{
                    "is_reusable": True,
                    "url": link
                }
            }
        }
    }
    out = requests.post(
        "https://graph.facebook.com/v2.6/me/message_attachments",
        json=message_template,
        params=auth_args
    ).json()
    print("Att_id: ", out)
    # add the attachment id provided by fb to DB
    db.session.add(Attachment(att_id=str(out['attachment_id']), att_url=link))
    db.session.commit()
    return str(out['attachment_id'])


def send_media(recipient_id, media_type, content, with_id=False):
    """
    Sends an image with or without buttons to fb
    :param recipient_id:
    :param media_type: "<image|video>",
    :param content: {'url': "https://nice.site.com/myimage.jpg", 'buttons': ["B1", "B2"]}
    :param with_id: if this is set to True it uses the url from content as it is without getting an id from fb
    :return:
    """
    link = content['url']
    # checks if it has to get an id from fb
    if not with_id:
        link = get_attachment_id(media_type, content['url'])

    # add buttons to the image if the content dict contains buttons
    buttons_list = []
    if content.get('buttons'):
        code = 0
        print("send_media with btn")
        for bts in content['buttons']:
            buttons_list.append(postback_button(bts, code))
            code += 1

    # create the template
    message_template = {
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "media",
                    "elements": [
                        {
                            "media_type": media_type,
                            "attachment_id": link,
                            "buttons": buttons_list or None
                        }
                    ]
                }
            }
        }
    }

    # send request
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_MEDIA".format(out.status_code), end='')
    if out:
        print(" ->", out.json())


def send_file(recipient_id, media_type, content, ):

    link = get_attachment_id(media_type, content['url'], reusable=False)

    message_template = {
      "recipient":{
        "id":recipient_id
      },
      "message":{
        "attachment":{
          "type":"file",
          "payload":{
            "attachment_id": link
          }
        }
      }
    }

    out = requests.post(
        "https://graph.facebook.com/v2.6/me/messages",
        json=message_template,
        params=auth_args
    ).json()
    print("Output: ", out)


def send_message(recipient_id, content):
    """
    Send simple message to fb
    :param recipient_id:
    :param content: {text: "message"}
    :return:
    """
    # create the template
    message_template = {
        'recipient': {
            'id': recipient_id
        },
        'notification_type': 'REGULAR',
        'message': {
            'text': content['text']
        }
    }
    # send request
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_MESSAGE".format(out.status_code), end='')
    if out:
        print(" ->", out.json())


def send_quickreplies(recipient_id, content, ret=False):
    """
    Send quick replyes to fb
    :param recipient_id:
    :param content: {'text': "message", 'buttons': ["ANS_1", "ANS_2", "ANS_3"], 'payload': [12, 243, 23]}
    :return:
    """

    replies = []
    if content.get('payload'):
        for index in range(len(content['buttons'])):
            replies.append(web_url_reply(content['buttons'][index], (content['payload'][index])))
    else:
        code = 0
        for button_text in content['buttons']:
            if content.get('url') and code in content['url']:
                replies.append(web_url_reply(button_text, code, content['url'][code]))
            else:
                replies.append(web_url_reply(button_text, code))
            code += 1

    # only build replies
    if ret:
        return replies

    # create the template
    message_template = {
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "text": content['text'],
            "quick_replies": replies
        }
    }

    # send request
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_QUICKREPLIES".format(out.status_code), end='')
    if out:
        print(" ->", out.json())


# TOneverDO: Send template mesages to FB
def generic_template(title=None, image_url=None, subtitle=None, url=None, webview_height_ratio="compact", buttons=None):
    generic_template_element = {
        "title": title,
        "image_url": image_url,
        "subtitle": subtitle,
        "default_action": {
            "type": "web_url",
            "url": url,
            "webview_height_ratio": webview_height_ratio
        },
        "buttons": buttons
    }

    for key in list(generic_template_element.keys()):
        if generic_template_element[key] == None:
            del generic_template_element[key]

    if url == None:
        del generic_template_element["default_action"]

    return generic_template_element


def send_template(recipient_id, content):
    """
    :parameter: content[0] -> templates
    :parameter: content[1] -> quick replies
    :exception: Daca nu sunt specificati toti parametrii in content nu se trimite mesaj catre user
    """
    template = []
    index = 0
    i = 0
    # print(content)
    for item in content[0]:
        template.append(generic_template(
            item['title'],
            item['image_url'],
            item['description'],
            item['url_redirect'],
            item['HEIGHT_RATIO'],
            item['buttons']
        ))
        # get_button
        template[index]['buttons'] = []
        for bts in item['buttons']:
            if bts.get('url'):
                template[index]['buttons'].append(web_url_button(bts['url'], bts['title']))
            else:
                i += 1
                if bts.get('payload'):
                    template[index]['buttons'].append(postback_button(bts['title'], bts['payload']))
                else:
                    template[index]['buttons'].append(postback_button(bts['title'], i))
        # from pprint import pprint
        # pprint(template[index])
        index += 1

    message_template = {
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template,
                }
            },
            "quick_replies": send_quickreplies(0, content[1], ret=True) if len(content) >= 2 else None
        }
    }
    out = requests.post(url, json=message_template, params=auth_args)
    print("[{}] FB_SEND_TEMPLATE".format(out.status_code), end='')
    if out:
        print(" ->", out.json())