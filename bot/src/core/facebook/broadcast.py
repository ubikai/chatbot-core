from core.facebook import auth_args
import requests
import json


def create_broadcast(text):
    url = "https://graph.facebook.com/v2.11/me/message_creatives"
    broadcast_template = {
        "messages":[
            {
                "dynamic_text": {
                    "text": text,
                    "fallback_text": "Hello friend!"
                }
            }
        ]
    }
    answer = requests.post(url, json=broadcast_template, params=auth_args).json()
    return answer['message_creative_id']


def send_broadcast(b_id, time = None):
    url = "https://graph.facebook.com/v2.11/me/broadcast_messages"
    broadcast_template = {
        "message_creative_id": b_id,
        "notification_type": "REGULAR",
        "messaging_type": "MESSAGE_TAG",
        "tag": "NON_PROMOTIONAL_SUBSCRIPTION",
        # TODO: add time scheduling
    }

    answer = requests.post(url, json=broadcast_template, params=auth_args).json()
    print("ANSWER IS: ", answer)


def get_broadcast_status(id):
    args = {
        'fields': "status,scheduled_time",
        'access_token': auth_args['access_token']
    }
    url = "https://graph.facebook.com/v2.11/{}".format(id)

    ans = requests.get(url, params=args)
    print(url)
    print(ans.json())


def get_all_broadcasts():
    url = "https://graph.facebook.com/v2.11/me/broadcast_messages?fields=scheduled_time,status&access_token={}".format(auth_args['access_token'])
    broadcasts = json.loads(requests.get(url).text)
    #print(type(ans))
    print("All boradcasts")
    print(json.dumps(broadcasts, indent=4))
    return broadcasts


def cancel_broadcast(id):
    operation = {
          "operation": "cancel"
        }
    url = "https://graph.facebook.com/v2.11/{}?access_token={}".format(id, auth_args['access_token'])
    ans=requests.post(url, json=operation)
    print(ans.json())


def cancel_all_broadcasts():
    broadcasts = get_all_broadcasts()
    ids = 0
    for broadcast in broadcasts['data']:
        ids += 1
        print(broadcast['id'])
    print(ids)


if __name__ == "__main__":
    pass
