from app import app
import requests

auth_args = {
  'access_token': app.config['ACCESS_TOKEN']
}

url = "https://graph.facebook.com/v2.6/me/messages"
