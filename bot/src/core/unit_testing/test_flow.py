import sys

sys.path.append("/bot/src")

import os
from core.chatbot.models import User
from core.chatbot.respond import start_answer
from core.memcache import load_flows
from time import sleep
from core.memcache import disengage_user, engage_user
from core.unit_testing import Raul


class TestFlows:
    os.environ['UNIT_TESTING'] = "1"

    user = User.get_user(Raul)

    def test_flow(self):
        messages = [
            {'message': 'delete_me', 'payload': 0},
            {'message': 'hello boy', 'payload': 0},
            {'message': 'no', 'payload': 0},
            # {'message': 'dsadsasa sasadsa', 'payload': 1},
        ]
        self.start_conversation(messages)

    def start_conversation(self, messages, **kwargs):
        load_flows()

        self.user.flow.reset()
        if kwargs.get("user_engaged"):
            engage_user(self.user.fb_id)
        else:
            disengage_user(self.user.fb_id)
        for message in messages:
            print(message)
            sleep(0.5)
            start_answer(self.user, message)

        disengage_user(self.user.fb_id)


TestFlows().test_flow()
