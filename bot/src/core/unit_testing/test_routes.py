import sys; sys.path.append("/bot/src")
import requests
from time import sleep
from multiprocessing import Process
from core.unit_testing import Raul
from helpers import test_unit_functions
from core.chatbot.models import User


class TestRoutes:
    def test_broadcast(self):
        params = {
            'user_id': 1,
            'message': 'Mesaj din broadcast',
            'message_type': 'message',
        }
        out = requests.post("http://localhost/broadcast", json=params)
        assert out
        if out.json().get('ERROR'):
            print(out.json())
            return "return_fail"

        user = User.query.filter(User.id == params['user_id']).first()
        assert user.temp['broadcast']['message'] == params['message']

    def test_facebook(self):
        def send_fb_route(text, user):
            msg = {'object': 'page', 'entry': [{'time': 1537619034597, 'id': '1430242947103967', 'messaging': [{'message': {'text': text,'seq': 541297,'mid': 'aO0qxyibLPRwfMSs3rkZ-l0WLqjKOuRIY8kLw7g7o1QoBrwOT87Alhcrw2fvKm0qQZ5PlGzyldfvET3W3wje1g'},'recipient': {'id': '1430242947103967'},'timestamp': 1537619033246,'sender': {'id': user}}]}]}
            requests.post("http://localhost/facebook", json=msg)
        msg = ['hi', 'stop']
        for i in range(len(msg)):
            t = Process(target=send_fb_route, args=(msg[i], Raul))
            t.start()
            sleep(1.5)

    def test_facebook_special_flow(self):
        """
        You must have an User in db with id=params['user_id']
        """
        params = {
            'user_id': 1,
            'action': 'get_started'
        }
        out = requests.post("http://localhost/facebook/special_flow", json=params)
        assert out
        if out.json().get('ERROR'):
            print(out.json())
            return "return_fail"

    def test_facebook_special_flow_temp(self):
        """
        You must have an User in db with id=params['user_id']
        """
        params = {
            'user_id': 1,
            'action': 'get_started',
            'temp': {'keyword': 'unit_test', 'value': 1}
        }
        out = requests.post("http://localhost/facebook/special_flow", json=params)
        assert out
        if out.json().get('ERROR'):
            print(out.json())
            return "return_fail"
        user = User.query.filter(User.id == params['user_id']).first()
        assert user.temp[params['temp']['keyword']] == params['temp']['value']

    def test_refresh_flows(self):
        out = requests.get("http://localhost/refresh_flows")
        assert out
        if out.json().get('ERROR'):
            print(out.json())
            return "return_fail"

    def test_logs(self):
        out = requests.get("http://localhost/logs")
        assert out
        if out.json().get('ERROR'):
            print(out.json())
            return "return_fail"

    def test_ad_request(self):
        # INFO: untested
        msg = {'object': 'page', 'entry': [{'time': 1550745126073, 'id': '2095824040433165', 'messaging': [{'recipient': {'id': '999999999'}, 'sender': {'id': Raul}, 'postback': {'referral': {'ad_id': '6110272717894', 'source': 'ADS', 'ref': 'asus', 'type': 'OPEN_THREAD'}, 'title': '\xc3\x8encepe', 'payload': 'GET_STARTED'}, 'timestamp': 1550745126073}]}]}
        assert requests.post("http://localhost/facebook", json=msg)

    def test_localhost(self):
        assert requests.get("http://localhost/").json()


if __name__ == "__main__":
    test_unit_functions(TestRoutes(), title='Default Routes Tests')
    # TestRoutes().test_ad_request()
