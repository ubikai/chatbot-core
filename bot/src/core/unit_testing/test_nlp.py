import sys; sys.path.append("/bot/src")
import dialogflow_v2 as dialogflow
from helpers import test_unit_functions
from os import environ
from core.google.api_ai import detect_intent, get_params
from datetime import datetime
from dateutil import parser

language_code = environ.get('LANG_CODE') or 'en-US'
project_id = environ['PROJECT_ID']

client = dialogflow.SessionsClient()


class TestNlp:
    def test_greeting(self):
        assert detect_intent(1, 'Hey') == 'greeting'

    def test_random_text(self):
        assert detect_intent(1, 'cacat pishat iaoan') == 'default.fallback'

    def test_get_datetime_context(self):
        params = get_params(1, '2 years ago', 'get_datetime', ['get_datetime'])
        date = params.fields['date-time'].struct_value.fields['date_time'].string_value
        date = parser.parse(date, ignoretz=True)
        assert isinstance(date, type(datetime.now()))

    def test_well_done(self):
        assert detect_intent(1, 'Good job robot!') == 'default.conversational.well_done'

    def test_neg_bad(self):
        assert detect_intent(1, "Wtf, that's bad..") == 'default.neg_bad'


if __name__ == '__main__':
    test_unit_functions(TestNlp(), title='NLP Tests')
