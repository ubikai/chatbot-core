from test_nlp import TestNlp
from test_routes import TestRoutes
from test_fieldtypes import TestFieldTypes
from test_sql import TestDataBase

from helpers import test_unit_functions

tests = [TestRoutes(), TestNlp(), TestFieldTypes(), TestDataBase()]
multi_logs = []
# multi_logs = [{'logs': ['1/5 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_get_datetime_context', '2/5 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_greeting', '3/5 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_neg_bad', '4/5 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_random_text', '5/5 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_well_done'], 'title': 'Unit Testing'}, {'logs': ['1/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_ad_request', '2/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_broadcast', '3/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_facebook', '4/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_facebook_special_flow', '5/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_facebook_special_flow_temp', '6/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_localhost', '7/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_logs', '8/8 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_refresh_flows'], 'title': 'Unit Testing'}, {'logs': ['1/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_change_flow', '2/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_custom_field_types', '3/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_custom_var', '4/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_error', '5/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_greeting', '6/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_media', '7/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_send_button', '8/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_send_button_postback', '9/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_send_message', '10/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_send_quick_reply', '11/11 [\x1b[32m\x1b[1mOKAY :D\x1b[0m] test_template'], 'title': 'Unit Testing'}]
for test in tests:
    multi_logs.append(test_unit_functions(test, verbose=False))

print('\n', '=' * 50)
print('Testing: ', *[(str(test).split('.')[0])[1:] + '.py' for test in tests])
print('-' * 50)
info = {'OKAY': 0, 'FAILED': 0}
for logs in multi_logs:
    for log in logs['logs']:
        if 'OKAY' in log: info['OKAY'] += 1
        if 'FAILED' in log: info['FAILED'] += 1
        print(log)
print('-' * 50)
print('OKAY:\t', info['OKAY'])
print('FAILED:\t', info['FAILED'])
print('=' * 50)
