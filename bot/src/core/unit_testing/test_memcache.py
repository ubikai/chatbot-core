from core.memcache import *


# TODO: Make a class filled with tests for memcached

def test_user_manager():
    user = "15"
    engage_user(user)
    print(user_engaged(user))
    mark_to_disengage(user)
    if check_to_disengage(user):
        print("user marked to disengage")
    disengage_user(user)


def test_flow_manager():
    print(read_flows())
    load_flows()
    print(get_flow("greeting"))
    print(get_flow("default.error"))


if __name__ == "__main__":
    # test_user_manager()
    load_flows()
