import sys

sys.path.append("/bot/src")

from core.chatbot.models import *
from app import db
from helpers import test_unit_functions


def test_get_user():
    print(User.query.get(1))


def test_add_analytics():
    user = User.query.get(1)
    print(user.add_analytics('bugs'))


def test_add_data():
    user = User.get_user("123")
    data = Data(category=DataCategory.get_data_category("contact"), keyword='phone', value='0743decurmaiei', user=user)
    data.submit()
    print(data)


class TestDataBase:
    def test_get_all_users(self):
        assert User.query.all()

    def test_set(self):
        dc = DataCategory.get_data_category('unit_test')
        db.session.add(dc)
        db.session.commit()

    def test_get(self):
        assert DataCategory.get_data_category('unit_test').id

    def test_from_user_to_flow(self):
        """
        You must have an User in db with id=params['user_id']
        """
        f_user = User.query.get(1)
        assert f_user.flow.current_flow


if __name__ == '__main__':
    test_unit_functions(TestDataBase(), 'DataBase Testing')
