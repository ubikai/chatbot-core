import sys

sys.path.append("/bot/src")

import os
from core.chatbot.models import User, db
from core.chatbot.respond import start_answer
from core.memcache import load_flows
from time import sleep
from core.memcache import disengage_user, engage_user
from core.unit_testing import Raul
from random import randint
from helpers import test_unit_functions


class TestFieldTypes:
    os.environ['UNIT_TESTING'] = "1"

    user = User.get_user(Raul)

    def test_send_button(self):
        random_quick_replay = [{'message': 'One', 'payload': 0}, {'message': 'Another One', 'payload': 1}]
        messages = [
            {'message': 'default.field_type.button', 'payload': 0},
            random_quick_replay[randint(0, 1)]
        ]
        self.start_conversation(messages)

    def test_send_button_postback(self):
        messages = [{'message': 'Descopera', 'payload': 'greeting'}]
        self.start_conversation(messages)

    def test_change_flow(self):
        messages = [{'message': 'default.field_type.change_flow', 'payload': 0}]
        self.start_conversation(messages)

    def test_custom_field_types(self):
        messages = [{'message': 'default.field_type.custom_field_type', 'payload': 0}]
        self.start_conversation(messages)

    def test_custom_var(self):
        messages = [{'message': 'default.field_type.custom_variable', 'payload': 0}]
        self.start_conversation(messages)

    def test_media(self):
        messages = [{'message': 'default.field_type.media', 'payload': 0}]
        self.start_conversation(messages)

    def test_send_message(self):
        messages = [{'message': 'default.field_type.message', 'payload': 0}]
        self.start_conversation(messages)

    def test_send_quick_reply(self):
        random_quick_replay = [{'message': 'One', 'payload': 0}, {'message': 'Another One', 'payload': 1}]
        messages = [
            {'message': 'default.field_type.quick_reply', 'payload': 0},
            random_quick_replay[randint(0, 1)]
        ]
        self.start_conversation(messages)

    def test_template(self):
        random_quick_replay = [{'message': 'One', 'payload': 0}, {'message': 'Another One', 'payload': 1}]
        messages = [
            {'message': 'default.field_type.template', 'payload': 0},
            random_quick_replay[randint(0, 1)]
        ]
        self.start_conversation(messages)

    def test_error(self):
        messages = [{'message': 'default.field_type.throw_error', 'payload': 0}]
        self.start_conversation(messages)

    def test_greeting(self):
        messages = [{'message': 'hi', 'payload': 0}]
        self.start_conversation(messages)

    # def test_ignore(self):
    #     messages = [
    #         {'message': 'asd', 'payload': 0},
    #         {'message': 'asd', 'payload': 0},
    #         {'message': 'asd', 'payload': 0},
    #         {'message': 'asd', 'payload': 0},
    #         {'message': 'Hi', 'payload': 0},
    #         {'message': 'asd', 'payload': 0},
    #         {'message': 'asd', 'payload': 0},
    #     ]
    #     self.start_conversation(messages)

    def start_conversation(self, messages, **kwargs):
        load_flows()

        analytics_init_value = self.get_analytics_value('interaction')
        self.user.flow.reset()
        if kwargs.get("user_engaged"):
            engage_user(self.user.fb_id)
        else:
            disengage_user(self.user.fb_id)
        for message in messages:
            print(message)
            sleep(0.5)
            start_answer(self.user, message)

        disengage_user(self.user.fb_id)
        analytics_final_value = self.get_analytics_value('interaction')
        assert analytics_init_value + len(messages) == analytics_final_value
        sleep(1.0)

    def get_analytics_value(self, analytics_type_name):
        db.session.refresh(self.user)
        analytics = self.user.analytics
        for entry in analytics:
            if entry.analytics_type.name == analytics_type_name:
                return entry.value

        return 0


if __name__ == '__main__':
    test_unit_functions(TestFieldTypes(), 'FieldType Tests')
