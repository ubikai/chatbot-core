print("Loading default field types")

# TODO: Automatizeaza asta in plm
#       Nu pot sa-l automatizez pentru ca exista module in def.field_types care importa alte module din def.field_types
from core.default_field_types.DefaultType import DefaultType
from core.default_field_types.ErrorType import ErrorType
from core.default_field_types.FlowType import FlowType
from core.default_field_types.FallbackType import FallbackType
from core.default_field_types.facebook_fields import *
