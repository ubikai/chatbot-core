from core.default_field_types import DefaultType
from core.facebook.messages import send_action
from time import sleep


class DefaultFacebook(DefaultType):
    def send_thinking(self):
        send_action(self.user.fb_id)
        sleep(self.state.delay)
        send_action(self.user.fb_id, "typing_off")
