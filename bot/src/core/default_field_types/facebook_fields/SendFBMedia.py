from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_media


class SendFBMedia(DefaultFacebook):
    def display_message(self, **kwargs):
        send_media(self.user.fb_id, self.state.content['type'], self.state.content)
