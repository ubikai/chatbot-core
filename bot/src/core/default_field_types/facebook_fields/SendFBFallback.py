from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.default_field_types import FallbackType
from core.facebook.messages import send_message


class SendFBFallback(DefaultFacebook, FallbackType):
    def display_message(self, **kwargs):
        self.format_message_intent()

        print('SendFBFallback txt', self.state.content['text'])
        send_message(self.user.fb_id, self.state.content)

