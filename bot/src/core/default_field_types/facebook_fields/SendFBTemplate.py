from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_template


class SendFBTemplate(DefaultFacebook):
    def display_message(self, **kwargs):
        send_template(self.user.fb_id, self.state.content)
