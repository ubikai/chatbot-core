from os.path import dirname, basename, isfile
import glob

files = glob.glob(dirname(__file__)+"/*.py")
modules = [basename(f)[:-3] for f in files if isfile(f) and not f.endswith('__init__.py')]
for module in modules:
    exec('from core.default_field_types.facebook_fields.{} import {}'.format(module, module))

__all__ = modules

