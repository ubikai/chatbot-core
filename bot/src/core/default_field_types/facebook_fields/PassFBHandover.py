from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_message
from core.facebook.handover import pass_handover_protocol


class PassFBHandover(DefaultFacebook):
    def display_message(self, **kwargs):
        send_message(self.user.fb_id, self.state.content)
        pass_handover_protocol(self.user.fb_id)
