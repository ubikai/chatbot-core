from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_message


class SendFBError(DefaultFacebook):
    def display_message(self):
        """
        :keyword error_data:
        :return:
        """
        content = self.state.content
        content['text'] = content['text'].format(self.kwargs['error_data'])
        send_message(self.user.fb_id, content)

