from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_file


class SendFBFile(DefaultFacebook):
    def display_message(self, **kwargs):
        send_file(self.user.fb_id, self.state.content['type'], self.state.content)
