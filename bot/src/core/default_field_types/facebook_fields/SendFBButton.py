from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_button


class SendFBButton(DefaultFacebook):
    def display_message(self, **kwargs):
        send_button(self.user.fb_id, self.state.content)
