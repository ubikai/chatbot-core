from core.default_field_types.facebook_fields.DefaultFacebook import DefaultFacebook
from core.facebook.messages import send_message


class SendFBMess(DefaultFacebook):
    def display_message(self, **kwargs):
        send_message(self.user.fb_id, self.state.content)
