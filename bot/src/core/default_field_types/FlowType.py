from helpers import DefaultError


class FlowType:
    def __init__(self, state, user, **kwargs):
        self.user = user
        self.state = state
        self.kwargs = kwargs
        # reset flow data and assign the new flow
        self.change_flow()

    def change_flow(self):
        print("Changing FLOW")
        self.user.flow.update_flows_completed(self.user.flow.current_flow)
        self.user.flow.reset()
        self.user.flow.current_flow = self.state.content['flow']
        index = self.state.content.get('index') or 0
        self.user.flow.get_flow_content()
        if not self.user.flow.flow_content:
            raise DefaultError("cannot get flow content for {}".format(self.user.flow.current_flow))
        self.state.change_state(self.user, self.user.flow.flow_content, index)
