from core.default_field_types import DefaultType


class ErrorType(DefaultType):
    def display_message(self):
        """
        :keyword error_data:
        :return:
        """
        content = self.state.content
        content['text'] = content['text'].format(self.kwargs['error_data'])
        print(content)
