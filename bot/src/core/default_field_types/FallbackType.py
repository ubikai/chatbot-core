from core.default_field_types import DefaultType
import emoji


class FallbackType(DefaultType):
    def format_message_intent(self):
        message = self.kwargs.get('message')

        if message:
            message = message['message']
            start_words = ["who", "what", "when", "where", "why", "how", "is", "can", "does", "do"]
            first_word = message.split(' ')[0].lower()

            if (first_word in start_words) or '?' in message:
                #  user send: question
                self.state.content['text_variation'] = self.state.content['text_question']
            else:
                message = message.replace(" ", "")
                if message[0] in emoji.UNICODE_EMOJI:
                    self.state.content['text_variation'] = self.state.content['text_emoji']
            self.random_text()
