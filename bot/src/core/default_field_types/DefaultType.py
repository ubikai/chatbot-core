import os
from app import app
from random import randint


class DefaultType:
    def __init__(self, state, user, **kwargs):
        self.user = user
        self.state = state
        self.kwargs = kwargs
        self.action_before_msg()

        self.random_text()

        if app.config['SEND_FB']:
            self.send_thinking()
            self.display_message()
        else:
            if state.field_type == "ErrorType":
                print("BOT: ", state.content['text'].format(self.kwargs['error_data']))
            else:
                print("BOT: ", state.content)

        self.action_after_msg()

    def display_message(self, **kwargs):
        print(self.state.content)

    def action_before_msg(self):
        pass

    def action_after_msg(self):
        pass

    def random_text(self):
        if type(self.state.content) == dict:
            if self.state.content.get('text_variation'):
                _max = len(self.state.content['text_variation'])
                self.state.content['text'] = self.state.content['text_variation'][randint(0, _max - 1)]
