import requests


def translate_text(text, language_code='en'):

    # text needs escaping to be safely used as URL '%&/'
    url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl={}&tl=en&dt=t&q={}"
    url = url.format(
            language_code,
            text
        )
    out = requests.get(url)
    if out:
        return out.json()[0][0][0]
    return text
