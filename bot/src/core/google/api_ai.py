import sys
import json
import dialogflow_v2 as dialogflow
import requests
from core.google.translate import translate_text

from os import environ

language_code = environ.get('LANG_CODE') or 'en-US'
project_id = environ['PROJECT_ID']

client = dialogflow.SessionsClient()


def detect_intent(session_id, text, contexts_names=None):
    """Returns the result of detect intent with texts as inputs.
    Using the same `session_id` between requests allows continuation
    of the conversaion."""
    nlp_out = query_nlp(session_id, text, contexts_names)
    if not nlp_out:
        return 'default.fallback'
    intent_name = nlp_out.intent.display_name
    return intent_name


def get_params(session_id, text, intent_name=None, contexts_names=None):
    nlp_out = query_nlp(session_id, text, contexts_names=contexts_names)

    if not nlp_out:
        return None
    elif nlp_out.intent.display_name != intent_name:
        return None
    elif not nlp_out.all_required_params_present:
        return None
    else:
        return nlp_out.parameters


def query_nlp(session_id, text, contexts_names=None):
    if len(text) > 256:
        return None

    session = client.session_path(project_id, session_id)

    # get query input
    if language_code != 'en-US':
        text = translate_text(text, language_code)
    text_input = dialogflow.types.TextInput(
        text=text, language_code='en-US')
    query_input = dialogflow.types.QueryInput(text=text_input)
    # print('Query input: {}'.format(query_input))

    # get query context
    if contexts_names:
        contexts = []

        for context_name in contexts_names:
            context_url = "projects/{}/agent/sessions/{}/contexts/{}".format(project_id, session_id,
                                                                             context_name)
            context = dialogflow.types.Context(name=context_url, lifespan_count=2)
            contexts.append(context)

        query_params = dialogflow.types.QueryParameters(contexts=contexts)
        # print('Query params: {}'.format(query_params))
    else:
        query_params = None

    response = client.detect_intent(
        session=session, query_input=query_input, query_params=query_params)
    # print("="*30)
    # print('Result: {}'.format(response.query_result))
    # print("="*30)
    # print(response.query_result, file=sys.stderr)
    return response.query_result
