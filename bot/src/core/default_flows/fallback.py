name = __file__

content = [
    {
        'index': 0,
        'content': {
            'text_variation': [
                "Didn't get that",
                "Didn't catch what you said",
                "Didn't understand that"
            ],
            'text_question': [
                "We'll come back to you",
                "I don't know the answer yet but we'll come back to you"
            ],
            'text_emoji': [
                "🤗",
                "😁",
                "😃"
            ]
        },
        'next_state': [0],
        'field_type': 'SendFBFallback',
        'expect_from_user': None,
        'delay': 0
    }
]
