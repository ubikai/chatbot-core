name = __file__

content = [
    {
        'index': 0,
        'content': {'text': "Changing the flow to SendFBMess"},
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
    {
        'index': 1,
        'content': {'flow': "default.field_type.message"},
        'next_state': [0],
        'field_type': 'FlowType',
        'expect_from_user': None,
        'delay': 0
    }
]