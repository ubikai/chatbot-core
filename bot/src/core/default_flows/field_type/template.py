name = __file__

content = [
    {
        'index': 0,
        'content': [
            [
                {
                    'title': "GAMES FPS",
                    'image_url': "https://i.imgur.com/VQi3MXC.png",
                    'description': None,
                    'url_redirect': None,
                    'HEIGHT_RATIO': "tall",
                    'buttons': [
                        {'title': "Vezi FPS", 'url': 'https://altex.ro/cauta/?q=GL504'},
                    ],
                },
                {
                    'title': "SPEC",
                    'image_url': "https://i.imgur.com/56WqgPU.png",
                    'description': None,
                    'url_redirect': None,
                    'HEIGHT_RATIO': "tall",
                    'buttons': [
                        {'title': "Youtube", 'url': 'https://www.youtube.com/results?sp=CAM%253D&search_query=gl504'},
                        {'title': "Google",
                         'url': 'https://www.google.ro/search?ei=QbnqW-LwKYuKmgWl65PABA&q=Asus+GL504+review&oq=Asus+GL504+review&gs_l=psy-ab.3..0i203j0i22i30l5.2511.6025..6176...3.0..1.194.1183.14j1......0....1..gws-wiz.......0j0i71j0i7i30j0i7i10i30j0i7i30i19j0i67.ZMztPP8jFRk'}
                    ],
                },
                {
                    'title': "COMPARA",
                    'image_url': "https://i.imgur.com/PKl9ZnX.png",
                    'description': None,
                    'url_redirect': None,
                    'HEIGHT_RATIO': "tall",
                    'buttons': [
                        {'title': "Compara", 'url': 'https://www.emag.ro/compare/DC7TZSBBM,DB90S4BBM,DWM5Z4BBM,D09L04BBM/'}
                    ],
                },
                {
                    'title': "BUY",
                    'image_url': "https://i.imgur.com/Nop4vyW.png",
                    'description': None,
                    'url_redirect': None,
                    'HEIGHT_RATIO': "tall",
                    'buttons': [
                        {'title': "Altex", 'url': 'https://altex.ro/cauta/?q=GL504'},
                        {'title': "Media Galaxy", 'url': 'https://mediagalaxy.ro/cauta/?q=GL504'}
                    ],
                }
            ],
            {
                'buttons': ['One', 'Another One']
            }
        ],
        'next_state': [1, 3],
        'field_type': 'SendFBTemplate',
        'expect_from_user': 'base.Postback',
        'delay': 0
    },
    {
        'index': 1,
        'content': {'text': 'You Chose One'},
        'next_state': [-1],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0,
    },
    {
        'index': 2,
        'content': {'text': 'If you see me, there is a problem with next_state -1'},
        'next_state': [-1],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0,
    },
    {
        'index': 3,
        'content': {'text': 'You Chose Another One'},
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0,
    },
]