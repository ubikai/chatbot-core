name = __file__

content = [
    {
        'index': 0,
        'content': {'text': "Choose one", 'buttons': ['One', 'Another One']},
        'next_state': [1, 2],
        'field_type': 'SendFBButton',
        'expect_from_user': 'base.Postback',
        'delay': 0
    },
    {
        'index': 1,
        'content': {'text': "You Chose One"},
        'next_state': [3],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
    {
        'index': 1,
        'content': {'text': "You Chose Another One"},
        'next_state': [3],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    }
]
