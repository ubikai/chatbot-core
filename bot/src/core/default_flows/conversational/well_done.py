name = __file__

content = [
    {
        'index': 0,
        'content': {
            'text_variation': [
                "It's my pleasure",
                "Glad to help",
                "Glad I could help"
            ]
        },
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
]
