name = __file__

content = [
    {
        'index': 0,
        'content': {
            'text_variation': [
                "Alright, thanks",
                "Glad to hear that",
                "I'm relieved, thanks"
            ]
        },
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
]
