name = __file__

content = [
    {
        'index': 0,
        'content': {
            'text_variation': [
                "It's my pleasure",
                "You're welcome",
                "I know you'd do the same for me",
                "No problem",
                "It's no bother",
                "Anytime",
                "Glad to help"
            ]
        },
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
]
