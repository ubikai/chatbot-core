class Postback:
    def __init__(self, message, user, state):
        self.message = message['message']
        self.user = user
        self.flow_data = user.flow
        self.payload = message.get('payload')
        self.state = state
        self.state.next_state[0] = self.get_next_state_index()
        state.expect_from_user = None

    def get_next_state_index(self):
        if type(self.state.content) == list:
            if len(self.state.content) > 1:
                buttons = self.state.content[1]
            else:
                buttons = None
        else:
            buttons = self.state.content.get('buttons')
        if buttons:
            next_state_index = int(self.payload)
        else:
            next_state_index = 0
        return self.flow_data.next_state[next_state_index]

