from core.google.api_ai import detect_intent, get_params


class NLP:
    def __init__(self, message, user, state):
        self.message = message['message']
        self.user = user
        self.flow_data = user.flow
        self.payload = message.get('payload')
        self.state = state
        self.nlp_out = None
        self.intent = None
        self.params = None

    def get_intent(self, context):
        self.intent = detect_intent(self.user.id, self.message, context)

    def get_params(self, intent_name=None, context_names=None):
        self.params = get_params(self.user.id, self.message, intent_name, context_names)


