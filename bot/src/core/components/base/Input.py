class Input:
    def __init__(self, message, user, state):
        self.message = message['message']
        self.user = user
        self.flow_data = user.flow
        self.payload = message.get('payload')
        self.state = state
        state.expect_from_user = None