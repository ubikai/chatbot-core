import sys

sys.path.append("/bot/src")
from os import listdir, path
from os.path import dirname, basename

current_folder = basename(dirname(__file__))


def get_list_of_files(dir_name):
    list_of_file = listdir(dir_name)
    all_files = list()

    for entry in list_of_file:
        full_path = path.join(dir_name, entry)
        if entry != "__init__.py" and entry != "__pycache__":
            if path.isdir(full_path):
                all_files = all_files + get_list_of_files(full_path)
            else:
                all_files.append(full_path[1:-3])

    return all_files


modules_paths = get_list_of_files(dirname(__file__))
modules = []

for path_index in range(len(modules_paths)):
    item = '.'.join(modules_paths[path_index].split('/'))
    item = item[item.index(current_folder):]
    modules.append(item)

contents = {}
errors = []

for mod in modules:
    mod_name = mod.replace(current_folder + '.', '', 1)
    import_statement = "from {} import content".format(mod)

    try:
        exec(import_statement)
        contents[mod_name] = str(content).encode("utf-8")
    except AttributeError as ex:
        err = "error importing {}".format(ex)
        errors.append(err)
    except ImportError as ex:
        err = "error importing {}".format(mod)
        errors.append(err)

if errors:
    print("[custom flows]ERRORS:")

    for error in errors:
        print(error)
