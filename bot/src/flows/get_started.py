name = __file__

content = [
    {
        'index': 0,
        'content': {'text': "in get_started flow"},
        'next_state': [1],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
    {
        'index': 1,
        'content': {'text': "{{user.first_name}}"},
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 2.0
    },
]