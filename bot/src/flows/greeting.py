name = __file__

content = [
    {
        'index': 0,
        'content': {'text': "Hello"},
        'next_state': [1],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 0
    },
    {
        'index': 1,
        'content': {'text': "msg 1 {{user.first_name}}"},
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 2.0
    },
    {
        'index': 2,
        'content': {'text': "msg 2"},
        'next_state': [0],
        'field_type': 'SendFBMess',
        'expect_from_user': None,
        'delay': 2.0
    }
]