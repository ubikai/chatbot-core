from subprocess import check_output
import sys


def dprint(*args):  # debug print
    print(*args, file=sys.stderr)


class DefaultError(Exception):
    """Base class for exceptions in this module."""

    def __init__(self, message):
        self.message = message


def test_unit_functions(test, title='Unit Testing', func_prefix='test', verbose=True):
    """
    Functions name with multiple words must be separed by a underscore (e.g test_my_func)

    :type test: object
    :param test: An object with test functions
    :param title: Result table name
    :param func_prefix: Exec all functions from :test: with that prefix in name
    :param verbose: Whatever or not you want to display the output or otherwise return it
    :return: dict(title=str, logs=dict)
    """
    colors = dict(reset='\u001b[0m', yellow='\u001b[33m', green='\u001b[32m', red='\u001b[31m', bold='\u001b[1m')

    def info(pos, func_name, fail=0):
        if fail == 1:
            color = colors['red'] + colors['bold'] + 'FAILED!' + colors['reset']
        elif fail == 'return_fail':
            color = colors['yellow'] + colors['bold'] + 'RETFAIL' + colors['reset']
        else:
            color = colors['green'] + colors['bold'] + 'OKAY :D' + colors['reset']
        msg = '{0}/{1} [{2}] {3}'.format(pos[0], pos[1], color, func_name)
        return msg

    logs = []
    funcs = [func for func in dir(test) if func_prefix == func.split('_')[0]]
    plen = int(len(max(funcs, key=len))) + 24
    txt = title + ' ' if int(len(title)) % 2 else title  # if title is even
    txt = txt + ' ' if plen % 2 else txt  # if plen is even
    spaces = ' ' * int(plen / 2 - len(txt) / 2 - 1)
    index = 0
    _max = len(funcs)

    for func in funcs:
        if func_prefix in func:
            index += 1
            try:
                out = eval('test.' + func)()
                if out == 'return_fail':
                    logs.append(info([index, _max], func, fail='return_fail'))
                else:
                    logs.append(info([index, _max], func))
            except:
                logs.append(info([index, _max], func, fail=1))
            sys.stdout.write(u"\u001b[1000D .")
            sys.stdout.flush()

    if verbose:
        print('\n\n' + '#' * plen + '\n#' + spaces + txt + spaces + '#\n' + '#' * plen)
        [print('# ' + log + ' ' * (plen - len(log) + 10) + '#') for log in logs]
        print('#' * plen + '\n\n')
    else:
        return {'title': title, 'logs': logs}


def get_container_ip(cont_name):
    out = check_output(["dig", "+short", cont_name])
    out = out.decode("utf-8")
    out = out.split('\n', 1)[0]
    return out if out is not "" else None
