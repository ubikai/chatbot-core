from core.default_field_types import DefaultType


class BroadcastType(DefaultType):
    def format_message_intent(self):
        message = self.kwargs.get('message')
        if message:
            self.state.content['text'] = self.user.temp['broadcast']['message']
            m_type = self.user.temp['broadcast']['message_type'].lower()

            if m_type == 'quickreplies' or m_type == 'input':
                if m_type == 'quickreplies':
                    self.state.content['buttons'] = self.user.temp['broadcast']['buttons']
                self.state.expect_from_user = 'SaveBroadcast'
                self.user.flow.expect_from_user = 'SaveBroadcast'
