from core.default_field_types import SendFBMess


class ExampleType(SendFBMess):
    def action_after_msg(self):
        print('>' * 50)
        print("[ExampleType] action_after_msg")
        print('<' * 50)
