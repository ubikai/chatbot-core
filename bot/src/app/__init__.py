import sys

sys.path.append("/bot/src")
from flask_api import FlaskAPI
from config import *
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os

print("ENV: ", os.environ.get("FLASK_ENV", default=False))

app = FlaskAPI(__name__)
if os.environ.get("FLASK_ENV", default=False) == 'production':
    app.config.from_object(ProductionConfig)
else:
    app.config.from_object(DevelopmentConfig)

print("TESTING: ", app.config["TESTING"])
db = SQLAlchemy(app)
migrate = Migrate(app, db)