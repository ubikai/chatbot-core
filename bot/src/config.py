from os import environ
from helpers import get_container_ip


class Config(object):
    APPLICATION_ROOT = 'app/'

    root_psw = environ.get("MYSQL_ROOT_PASSWORD", default=None)
    hostname = environ.get("MYSQL_HOSTNAME", default=None)
    db_name = environ.get("MYSQL_DATABASE", default=None)

    PROJECT_NAME = environ.get("PROJECT_NAME", default=None)

    SQLALCHEMY_DATABASE_URI = 'mysql://root:{}@{}/{}'.format(root_psw, hostname, db_name)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ACCESS_TOKEN = environ.get("ACCESS_TOKEN", default=None)
    VERIFY_TOKEN = environ.get("VERIFY_TOKEN", default=None)

    # Edit CHAT_BOT_NAME if you uana iuz da tt service
    TT_IP = get_container_ip('{}_timetracker_1'.format(PROJECT_NAME))


class DevelopmentConfig(Config):
    DEBUG = True  # Turns on debugging features in Flask
    TESTING = True
    SEND_FB = True


class ProductionConfig(Config):
    DEBUG = False  # Turns off debugging features in Flask
    TESTING = False
    SEND_FB = True
