from core.components.base.Input import Input
from core.components.base.Postback import Postback
from requests import post
from app import app
import emoji


class SendToRoute(Input, Postback):
    """
    <Flow.State example>

    'content': {
        'text': "How are you?",
        'buttons': ['ghj', 'tyu'], # optional
        'kw': 'feeling',
        'route': '/user/feeling'
    },
    """
    def __init__(self, message, user, state):
        if state.content.get('buttons'):
            Postback.__init__(self, message, user, state)
        else:
            Input.__init__(self, message, user, state)

        # remove emoji from messages
        for character in message['message']:
            if character in emoji.UNICODE_EMOJI:
                message['message'] = message['message'].replace(character, '')

        data = {
            'user_id': user.id,
            'keyword': self.state.content.get('kw') or '<undefined>',
            'value': message['message']
        }
        out = post("http://" + app.config['TT_IP'] + self.state.content.get('route'), json=data)
