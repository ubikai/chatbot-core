from core.components.base import Input
from core.chatbot.models import Data, DataCategory
import sys

# noinspection PyArgumentList
class SaveBroadcast(Input):
    def __init__(self, message, user, state):
        print("@"*40, file=sys.stderr)
        Input.__init__(self, message, user, state)
        self.save()
        print("@" * 40, file=sys.stderr)

    def save(self):
        print('SAVE keyword={}, value={}'.format(self.user.temp['broadcast']['message'], self.message))
        data = Data(
            category=DataCategory.get_data_category("broadcast"),
            keyword=self.user.temp['broadcast']['message'],
            value=self.message,
            user=self.user)
        # TODO: Trello #001
        # self.user.temp['broadcast ]= [77]
