#!/usr/bin/env bash
if [ ! -d "/bot/migrations" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  flask db init -d /bot/migrations
fi
flask db migrate -m "up" -d /bot/migrations
flask db upgrade -d /bot/migrations

python3 /bot/src/core/memcache/upload_flows.py

echo "" > /bot/logs/error.log
echo "" > /bot/logs/access.log

if test $FLASK_ENV = 'development'; then
    flask run --host=0.0.0.0 --port=80
else
    /usr/sbin/apache2ctl -D FOREGROUND
fi